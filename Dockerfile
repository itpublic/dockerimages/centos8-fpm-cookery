FROM centos:centos8

RUN yum -y update \
    && yum -y install epel-release \
    && yum -y install ruby ruby-devel rubygems rake \
    && yum -y install nodejs openssl rpm-build rsync which git \
    && yum -y install gcc gcc-c++ make openssh-clients wget ansible \
    && gem install -N fpm-cookery \
    && yum -y update && yum clean all

